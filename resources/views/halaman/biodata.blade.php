@extends('layout.master')
@section('judul')
Buat Account Baru!
@endsection
 
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
    @csrf
    <label form="nm_depan">First Name:</label><br>
        <input type="text" placeholder="" id="nm_depan" name="nama_dpn">
        <br><br>
        <label for="nm_belakang">Last Name:</label><br>
        <input type="text" placeholder="" id="nm_belakang" name="nama_blkg">
        <br><br>
        <label>Gender:</label>
        <br>
        <input type="radio" name="jns_kelamin" value="Male"> Malee
        <br>
        <input type="radio" name="jns_kelamin" value="Female"> Female
        <br>
        <input type="radio" name="jns_kelamin" value="Other"> Other
        <br><br>
        <label>Nationality</label>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="American">American</option>
            <option value="British">British</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bhs" value="Bahasa Indonesia"> Bahasa Indonesia
        <br>
        <input type="checkbox" name="bhs" value="English"> English
        <br>
        <input type="checkbox" name="bhs" value="Other"> Other
        <br><br>
        <label for="bio">Bio:</label>
        <br>
        <textarea id="bio" name="biodata" cols="30" rows="10"></textarea>
        <br>
    <input type="submit" value="kirim">
    </form>
@endsection