@extends('layout.master')
@section('judul')
List Pemain Film 
@endsection

@section('content')
<a href="/cast/create" class="btn-warning btn-sm mb-3">Tambah Data</a>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td><a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a></td>
                <td><a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">Edit</a></td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Masih Kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection