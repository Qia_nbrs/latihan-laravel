@extends('layout.master')
@section('judul')
Halaman Detail Pemain {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}}</h3>
<p>{{$cast->bio}}</p>
@endsection